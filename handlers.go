package main

import (
	"./models"
	"bytes"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"github.com/dgraph-io/badger"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

// needed for CORS (cross origin resource sharing) when making requests from within an browser
func preflightOK(c *gin.Context) {
	c.JSON(http.StatusOK, struct{}{})
}

// @Summary Post a block of binary data
// @Description Post a block of binary data between 1 byte and 64 MiB returning a SHA1 hash of the data posted is JSON format
// @ID post-block
// @Accept multipart/form-data
// @Produce json
// @Param block formData file true  "The block file you wish to upload"
// @Success 200 {object} models.SHA1Response
// @Failure 400 {object} models.APIStatusMessage
// @Failure 409 {object} models.APIStatusMessage
// @Failure 500 {object} models.APIStatusMessage
// @Router /api/v1/blocks/post-block [post]
func (a *App) postBlock(c *gin.Context) {
	blockData, err := c.FormFile("block")
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("An error occurred proccessing the form file 'block' Error : %v ",
				err.Error())})
		return
	}
	if blockData.Size > int64(a.config.Gin.MaxBlockSizeMib)<<20 {
		c.JSON(http.StatusBadRequest, models.APIStatusMessage{Code: http.StatusBadRequest,
			Message: fmt.Sprintf("block data must not be greater than %v MiB in length", a.config.Gin.MaxBlockSizeMib)})
		return
	}
	if uint64(blockData.Size) < a.config.Gin.MinBlockSizeBytes {
		c.JSON(http.StatusBadRequest, models.APIStatusMessage{Code: http.StatusBadRequest,
			Message: "block data must be greater than 1 byte in length"})
		return
	}
	// If all you wanted me to do is save the file to disk this is how I would do that
	// c.SaveUploadedFile(blockData, "some path")

	//c.SaveUploadedFile( blockData, "")

	// get the file out of the form data
	file, err := blockData.Open()
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("An error occurred opening the uploaded file 'block' Error : %v ",
				err.Error())})
		return
	}
	defer file.Close()

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("could not read file to byte array : Error : %v ",
				err.Error())})
		return
	}

	h := sha1.New()
	h.Write(fileBytes)
	hashBytes := h.Sum(nil)

	// write to DB

	// the transaction is supposed to return an error to one of the transactions
	// if multiple transactions attempt to modify the same value at the same time
	txn := a.db.NewTransaction(true)
	defer txn.Discard()

	// Please note that values returned from Get() are only valid while the transaction is open.
	// If you need to use a value outside of the transaction then you must use copy()
	// from the badger documentation
	val, err := txn.Get(hashBytes)

	if err == nil {
		c.JSON(http.StatusConflict, models.APIStatusMessage{Code: http.StatusConflict,
			Message: "The block data already exists"})
		return
	}
	if err != badger.ErrKeyNotFound {
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("Error  getting value from the db Error : %v ",
				err.Error())})
		return
	}
	// the key doesn't exist so we can create it
	err = txn.Set(hashBytes, fileBytes)
	// the transaction returns an error it could be because there was another go routine was editing the same value
	if err != nil {
		if err == badger.ErrConflict {
			c.JSON(http.StatusConflict, models.APIStatusMessage{Code: http.StatusConflict,
				Message: fmt.Sprintf("Error transaction conflict db Error : %v ",
					err.Error())})
			return
		}
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("Error setting key and value to the db Error : %v ",
				err.Error())})
		return
	}
	// the value is gone once the transaction is committed
	if val != nil {
		var forCompare []byte
		val.ValueCopy(forCompare)
		// I need to check for  an improbable sha1 collision or return an error that the data already exists
		if bytes.Compare(fileBytes, forCompare) != 0 {
			c.JSON(http.StatusConflict, models.APIStatusMessage{Code: http.StatusConflict,
				Message: fmt.Sprintf("We have an SHA1 collision! : %v ",
					err.Error())})
			return
		}
	}

	// if we ran out of disk space I should return the error
	// http.StatusInsufficientStorage

	txn.Commit()

	// finally return the sha1 hash as a string

	c.JSON(http.StatusOK, models.SHA1Response{Code: 200, Hash: fmt.Sprintf("%x", hashBytes)})
}

// @Summary Returns a block of data as a file
// @Description Given an SHA1 hash will return and validate data stored to disk in the form of a file
// @ID get-block
// @Produce application/octet-stream
// @Param sha1_hash path string true  "da39a3ee5e6b4b0d3255bfef95601890afd80709"
// @Success 200 string binary
// @Failure 400 {object} models.APIStatusMessage
// @Failure 500 {object} models.APIStatusMessage
// @Router /api/v1/blocks/get-block/{sha1_hash} [get]
func (a *App) getBlock(c *gin.Context) {
	hashString := c.Param("sha1_hash")

	hashBytes, err := hex.DecodeString(hashString)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.APIStatusMessage{Code: http.StatusBadRequest,
			Message: fmt.Sprintf("Unable to decode hash string : %v ",
				err.Error())})
		return
	}

	// open for read only
	txn := a.db.NewTransaction(false)
	defer txn.Discard()

	val, err := txn.Get(hashBytes)
	if err != nil {

		if err == badger.ErrKeyNotFound {
			c.JSON(http.StatusNotFound, models.APIStatusMessage{Code: http.StatusNotFound,
				Message: fmt.Sprintf("That has does not exist in the DB : %v ",
					hashString)})
			return
		} else {
			c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
				Message: fmt.Sprintf("Error getting value from DB : %v ",
					err.Error())})
			return
		}

		c.JSON(http.StatusBadRequest, models.APIStatusMessage{Code: http.StatusBadRequest,
			Message: fmt.Sprintf("Unable to decode hash string : %v ",
				err.Error())})
		return
	}
	returnBytes := []byte{}
	log.Println(val.EstimatedSize())

	returnBytes, err = val.ValueCopy(nil)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("Error copying value from DB item : %v",
				err.Error())})
		return
	}

	txn.Commit()

	h := sha1.New()
	h.Write(returnBytes)
	checkHashBytes := h.Sum(nil)

	// validate the data
	if bytes.Compare(hashBytes, checkHashBytes) != 0 {
		// may want to delete invalid data from db
		c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: http.StatusInternalServerError,
			Message: fmt.Sprintf("Data is corrupted ! The hash of the value in the DB does not match ! : %v ",
				err.Error())})
		return
	}

	// return valid block
	// these headers cause the file to download to disk
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", "attachment; filename=block-"+hashString)
	c.Header("Content-Type", "application/octet-stream")

	http.ServeContent(c.Writer, c.Request, hashString, time.Now(), bytes.NewReader(returnBytes))

}
