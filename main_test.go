package main

import (
	"github.com/dgraph-io/badger"
	"os"
	"testing"
)

// due to time constraints I have not written many of the tests that should have been written

// I wanted to show that I could write a basic test

func TestMain(m *testing.M) {
	code := m.Run()
	os.Exit(code)
}

func TestValidConfig(t *testing.T) {
	_, err := getConfig()
	if err != nil {
		t.Errorf("Config failed to load %v", err.Error())
	}
}

func TestDBAccessible(t *testing.T) {
	config, err := getConfig()
	if err != nil {
		t.Errorf("Config failed to load %v", err.Error())
	}
	valDir := config.Badger.ValueLogDir
	optsDir := config.Badger.OptsDir

	_, err = os.Stat(valDir)
	if err != nil {
		t.Errorf("Badger value log directory does not exist")
	}
	_, err = os.Stat(optsDir)
	if err != nil {
		t.Errorf("Badger opts directory does not exist")
	}

	//  open the db
	opts := badger.DefaultOptions
	opts.Dir = optsDir
	opts.ValueDir = valDir
	db, err := badger.Open(opts)
	if err != nil {
		t.Errorf("Badger DB failed to open %v", err.Error())
	}
	defer db.Close()

}

func TestPostBlock(t *testing.T) {

}

func TestGetBlock(t *testing.T) {

}

/*
func TestPostEmptyBlock(t *testing.T){
	config, err := getConfig()
	if err != nil {
		t.Errorf("Config failed to load %v", err.Error())
	}
	//http https should be stored in the config
	uri := fmt.Sprintf("http://%s:%v", config.Gin.Host, config.Gin.Port)

	response, err := postRandomBlock(uri, 0)
	if err != nil {
		t.Errorf("Request failed %v", err.Error())
	}

	checkResponseCode(t, http.StatusOK, response.Code)

	body := response.Body.String()

	log.Printf("%v", body)
}


func TestPostTooLargeBlock(t *testing.T){
	config, err := getConfig()
	if err != nil {
		t.Errorf("Config failed to load %v", err.Error())
	}	   //http https should be stored in the config
	uri := fmt.Sprintf("http://%s:%v", config.Gin.Host, config.Gin.Port)
	req, err := postRandomBlock(uri, 0)
	if  err != nil{
		t.Errorf("Error posting block %v", err.Error())
	}
	log.Printf("%v", req.Body)
}

func postRandomBlock(uri string, size uint64) (*httptest.ResponseRecorder, error){
	block := generateRandomBytes(size)

	body := bytes.NewBuffer(block)
	writer := multipart.NewWriter(body)
	req, err := http.NewRequest("POST", uri, body)
	if  err != nil{
		return nil, err
	}
	req.Header.Set("Content-Type", writer.FormDataContentType())
	// response :=
	return response, err


}


func generateRandomBytes(len uint64) []byte{
	randBytes := make([]byte, len)
	rand.Read(randBytes)
	return randBytes
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

*/

func getConfig() (*Config, error) {
	config, err := NewConfig()
	if err != nil {
		return nil, err
	}
	return config, nil
}
