package main

import (
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// I've gotten in the habit of breaking my routes into a separate file
// If I had many routes I would likely break  them into groups in separate files
func GetRouter(a *App) *gin.Engine {
	router := gin.Default()

	// adding in the swagger UI
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	// the group path contains the version of the api and refers to the name of relevant object
	blocks := router.Group("api/v1/blocks")
	{
		// If this api will be used in a web browser Options request needs to be handled with at least a 200
		// This is related to CORS  https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

		blocks.POST("post-block", a.postBlock)
		blocks.OPTIONS("post-block", preflightOK)

		blocks.GET("get-block/:sha1_hash", a.getBlock)
		blocks.OPTIONS("get-block/:sha1_hash", preflightOK)

		// put for update

		// delete for delete
	}
	return router
}
