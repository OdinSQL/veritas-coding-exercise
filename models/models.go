package models

// The swagger generator is supper picky about formatting of the json hint

// I was having an issue with the generator recognizing these structs
// I ended up putting these models in a separate package so that that the generator would finally recognize them

// a different type could be created for each http status message
type APIStatusMessage struct {
	Code    uint16 `json:"code" example:"200"`
	Message string `json:"message" example:"Everything is great!'"`
}

type SHA1Response struct {
	Code uint16 `json:"code" example:"200"`
	Hash string `json:"sha1-hash" example:"cf23df2207d99a74fbe169e3eba035e633b65d94"`
}
