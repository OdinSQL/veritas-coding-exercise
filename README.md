# Benjamin Knigge's veritas coding exercise

## What Veritas is looking for

* Your program should work gracefully in a multithreaded or multiprocessing environment. Be sure to consider threading
 issues and race conditions. Please be prepared to address conditions that include:
    * Concurrent writes
    * Writes concurrent with reads
    * Disk full
    * Nonexistent data requested
* Packaging (e.g. setup.py if you choose Python) is not important for this exercise
* Tests are important
* Good API design is important. We use RESTful design patterns.
* Document how to run the code in comments or a README file
* Document your assumptions and trade-offs in comments or a README file
* Prioritize code clarity and maintainability over performance
* Use of external libraries & modules is fine


## Description of the problem

* Write an HTTP service in Ruby, Python, or Go.
    * Support the following transactions:
        * POST - write request body (a ‘block’ - anywhere from 1 byte to 64 MiB) content to disk. 
        Respond with sha1 hash of data.

        * GET - given a sha1, respond with data previously POSTed
    * Cases to consider / test:
        * Write a block
        * Read a block (verify sha1)
        * Concurrent writes
        * Concurrent write with same data content
        * Disk full
        * Nonexistent data requested
        * Write a block that already exists on disk
        * Data corrupt on disk


## Thoughts and notes regarding design decisions
I'm writing this document outlining the problem and my thoughts prior to beginning writing any code.
These are my thoughts formatted and I haven't been counting the time to write this down as part of the not more than 
four hours of implementation.


When speaking with Ward regarding this coding exercise he told me something along the lines of "Do it how you normally 
would do it... Make it look nice ... You will need to explain your decisions in a code review"
That's definitely not an exact quote but that's what is driving my decisions.

Typically I would ask 1001 questions regarding the intention and purpose behind what I have been asked to do.


### Why I've chosen Go 

 * I've been working primarily in go for the past couple of years and I have a strong preference for compiled statically
  typed languages. 
 Some of the reasons why include:
 
    * Type hints from withing my development environment
    * Discovery of many bugs at the time of compilation rather than in production or QA
    * Some degree of input validation
    * Generally superior runtime performance when compared to non-compiled languages
    
* Go has been designed to be inherently multi threaded. Some examples would include:
    * Goroutines - Go's lightweight thread
    * Channels - Go's conduit for communicating with a go routine
    * Go's http server spawns go routines for each request.
    * Since version 1.6 By default go will utilize every core available for running go routines. This can be controlled 
    via the GOMAXPROCS variable.

### How I would normally handle this?
I would normally use a modern relational database that supports concurrent reads and writes and a journaling transaction
 log.
 * The key would have a unique constraint applied to it
 * The block would be stored as a blob/text field
 * I would not use an SHA1 hash as a key if at all possible, as it is not guaranteed to be unique. A collision is
  unlikely however my preference would be to use an int of a large enough size or a GUID. An int would have performance
  advantages over a GUID and GUID would have advantages when it comes to replication involving multiple masters writing
  to the same table. The fact that the key is not unique means that we will have to do a comparison of the existing
  string and any potentially duplicate data prior to returning an error that the data is duplicate or that a collision 
  has occurred. 

### Notes regarding GOMAXPROCS
I've previously benchmarked applications with GOMAXPROCS set to a number significantly higher than the actual number of
 CPU's available.Applications that are heavily IO dependant can reportedly benefit the most from setting this value 
 higher. Testing would be necessary to determine the ideal setting. I've previously read the number of CPU's and 
 multiplied by between 2 and 8 and found it to improve performance in some cases.
  
### Ways to avoid or minimize SHA1 collision 
 * The ideal solution would be to use a unique value as the key ( 64bit int or GUID)
 * Use a unique value along with the sha1 hash as the key
 * Minimize the chance of SHA1 collision by using SHA256 or SHA512 instead. A collision would still be possible but would be less likely.

### Data structures that I have considered before ultimately rejecting and why

#### Using a relation database server
I briefly though about using an external relational database but have decided against it. I feel that it would violate
the rules outlined. Using Postgres would be more than an external library or module.

#### Using an embedded relational database
SQLLite can be embedded into a go application but I've decided to limit myself to only libraries and modules written in 
go

#### Using Go's sync Map https://golang.org/src/sync/map.go

I had considered using Go's sync Map type which is thread safe with a value of a struct containing the path to the file 
containing the block data and a bool. 

  * Pending would be true until after the file had been written without error at which point
the map entry would be updated.

  * If two posts occurred simultaneously one of the two would throw an error (see sync Map LoadOrStore ) stating that an
   existing post was pending if the pending bool was true, or a duplicate error if it was not pending.

  * if an error occurred writing the block data to the file the entry would be deleted.
  
  * Pending necessary for returning an error in the case of simultaneous writes with the same data and in the case of the
   program ending abruptly while in the process of writing a block in which case a pending entry could be validated upon
    restarting the application.
  
    
Example struct considered for BlockFileData :
    
    type BlockFileData struct {
        FilePath string
        Pending bool
    }
  
Why I have decided against using sync Map
  * It would not scale very well
    * Storing potentially millions of hashes and file paths would consume an enormous amount of memory.
    * Reliably copying and serializing a map of potentially millions of entries would be resource intensive and error 
    prone ( I would need to write a journaling transaction log to handle this issue effectively ) 
  * The map could not be reliably reconstructed from the files containing the blocks alone in the case of corruption of 
  the serialized map unless  the SHA1 key was also being stored along with it. There would be no way to determine if the
   data was corrupted without it.
    
  * I would spend much of the four hours allotted for writing a web service and tests obsessing over the best way to 
  reliably serialize a potentially enormous map.

#### bbolt - An embedded key/value database for Go.

https://github.com/etcd-io/bbolt

##### Advantages
* It is a package written in Go
* This is basically the same idea as the sync map
* I won't have to deal with the memory issues related to potentially millions of entries in a map
* I won't have to spend time trying to figure out the perfect way to serialized a map quickly minimizing the chance of 
corruption
* The package has been well tested in production and has over 100 contributors

##### Disadvantages
* I've never used it before
* It does not support concurrent writes ( I would need to use a mutex to put a lock around a write function )

##### Notes

If a duplicate value is "Put" into a bucket the error ErrIncompatibleValue will be returned 
( line 304 https://github.com/boltdb/bolt/blob/master/bucket.go)
  

### The idea I've decided to go with for in regard to a data structure

#### badger - Fast key-value DB in Go. 

https://github.com/dgraph-io/badger

##### Advantages
 * Same as with bbolt
 * Disk errors should propagate up ( https://github.com/dgraph-io/badger/issues/23 )
 * Supports concurrent reads and writes
 
##### Disadvantages
* I've never used it before
* It's not as mature as bbolt
* Dealing with transactions errors that should occur when checking the value of a key prior to insertion 
    is more complicated. ( I should write a valid test but will I have time? )



### Why I've decided against storing each block in it's own file
I had considered writing each block to it's own file and storing the path in the map/key value store
however there is also a default inode limit per directory on the EXT4 file system of 64,000 
additionally there is an inode limit per partition that can be adjusted at the time of partitioning
For these reasons I've decided to store the block data in the bbolt. If the intention was to have me write each block to
to an individual file. 


### Thoughts related to HTTP server

#### Go's http.Server 

https://golang.org/pkg/net/http/?

One option would be to use Go's default http.Server 
 
 and pairing it with a router like gorilla/mux. https://github.com/gorilla/mux

This would be a perfectly fine approach and the primary reason that I did not choose this approach is that I'm more
 familiar with Gin and have used it in project in the past.

#### Gin 
https://gin-gonic.github.io/gin/

I've used Gin in the past for similar tasks involving REST APIs.

##### Why Gin
* It's fast
* Built in JSON validation
* Built in Router
* Extensive existing Middleware
* Automatic crash recovers
* Well documented
* Well tested
* Large user base
* I've used it in the past and am familiar with it


#### Other web development frameworks

There are many other web frameworks that I have evaluated in the past and the only other "framework" I would likely
 consider currently based on it's documentation and the size of it's development community would be Echo.

https://github.com/labstack/echo


### Ideas related to Security

Security was not mentioned but it's always a concern. Here's what I've done in the past.

#### TLS

SSL in now ubiquitous. I would normally configure a servers running as a system service
to run on a unix socket or on local host and then put NGINX
in front of it to handle request logging, load balancing, fail over and TLS.

I'm aware  of other options for load balancing and that I can configure Go's http server to use https.

I'm more familiar with NGINX and trust that it's reliable.

I Also frequently put everything behind CloudFlare which is a DNS based Content distribution network that does a 
pretty good job of filtering out malicious bots 

#### Json Web Tokens 

https://jwt.io

Json web tokens seem to be the new standard in securing REST APIs.

I have used key pairs in the past to sign tokens and secure APIs.

One of the main advantages is that they allow you to minimize the database calls required to repeatably
validate a users access. An authenticated uses roles can be securely stored in the token.

### Ideas related to Configuration

I've gotten in the habit of creating a TOML https://github.com/toml-lang/toml config file for my applications
 and loading it into a "config" struct. Any variable that is potentially going to be altered  at runtime will be read
  from the TOML config.
I prefer TOML to YAML or JSON as I find it simpler than either.

## Documentation with swagger
The swagger UI allows for the documentation and testing of REST APIs via a web interface making use of swaggo 
declarative comments. https://github.com/swaggo/gin-swagger

### Thoughts related to encoding
How the data in the request body was to be encoded was not specified. Files posted to an api are frequently encoded 
in base64 but that isn't very efficient for files up to 64 MiB

## Packaging
I had considered using go dep but decided against it.
 I'm not sure if you are using it, it would take a small amount of time and the instructions state that it is not necessary.
You will have to "go get" the dependencies. 

## Dependencies

I'm currently working with Go 1.11 and if compiling would recommend using the same version.

This code should be compatible with go versions 1.9 and newer but has only been testing using go 1.11

> go get -u

For each of the following dependencies

* "github.com/gin-gonic/gin" - web frame work

* "github.com/BurntSushi/toml" - TOML config support

* "github.com/swaggo/swag/cmd/swag" - swagger command line tool

* "github.com/swaggo/gin-swagger" - gin-swagger middleware

* "github.com/swaggo/gin-swagger/swaggerFiles" - swagger embed files

* "github.com/dgraph-io/badger/..." - badger Fast key-value DB in Go

* "github.com/gin-contrib/size" - gin middleware for a post data size limit


## How to run

> git clone https://gitlab.com/OdinSQL/veritas-coding-exercise.git

cd to the veritas-coding-exercise directory

Run the app by typing

> go run .

open your browser to 

> http://localhost:8182/swagger/index.html 

or to the host and port that you have configured


## Updating swagger documentation
from within the application directory run

> swag init

## Tests

Tests are important but take time to write. 

### I plan on writing as many of the following tests that time allows.
* Swagger server is up and running
* Post containing empty block returns appropriate error
* Post containing block that is too long returns appropriate error
* Posting duplicate content returns appropriate error
* Requesting block after posting returns expected data

### Tests that I do not plan on writing due to time constraints

Both instances have been considered and should result in the error being returned.

* SHA1 collision https://shattered.io/

An SHA1 collision is possible but improbable and writing the test could potentially take me more time than is available.
I want to at least create a functioning REST API and a few  tests of the basic functionality. 
If time allows I may create this test.

* Concurrent writing the same data

This should result in one of the calls writing the data succeeding and all other concurrent calls failing.
If time allows I may create this test.

* Disk full 

I had thought about creating a script to generate virtual disk and then filling it for the purpose of a test but have
 decided that it will likely take up too much of the 4 hour development time writing the script, the test, and testing
  the test.
  
### How to run tests

> go test -v

Due to time constraints I did not write as many tests as I could have. 
I wanted to demonstrate that I could write a test without spending too much additional time.
If you would like to see more tests I would be open to creating them.

## Race Conditions

Race conditions should not be an issue as I've decided to use a thread safe key value database that supports transactions.

You can be tested for by using the -race command line switch

> go test -race -v









    

    

    


