package main

import (
	"github.com/BurntSushi/toml"
)

type Config struct {
	Title     string
	Verbose   bool
	GoMaxProc GoMaxProc
	Gin       Gin
	Badger    Badger
}

type GoMaxProc struct {
	Auto     bool
	Override uint16
}

type Gin struct {
	DebugMode         bool
	Host              string
	Port              uint16
	MaxBlockSizeMib   uint16
	MinBlockSizeBytes uint64
}

type Badger struct {
	OptsDir     string
	ValueLogDir string
}

func NewConfig() (*Config, error) {
	var Conf Config
	if _, Err := toml.DecodeFile("config.toml", &Conf); Err != nil {
		return nil, Err
	}
	return &Conf, nil
}
