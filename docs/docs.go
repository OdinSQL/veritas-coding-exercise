// GENERATED BY THE COMMAND ABOVE; DO NOT EDIT
// This file was generated by swaggo/swag at
// 2018-10-01 22:03:27.082588334 -0600 MDT m=+0.037986422

package docs

import (
	"bytes"

	"github.com/alecthomas/template"
	"github.com/swaggo/swag"
)

var doc = `{
    "swagger": "2.0",
    "info": {
        "description": "This API is part of a Veritas coding exercise regarding a REST API to write and retrieve blocks of data",
        "title": "Veritas coding exercise",
        "contact": {
            "name": "Benjamin Knigge",
            "url": "https://odinsql.com/contact/",
            "email": "ben@heptec.com"
        },
        "license": {},
        "version": "1"
    },
    "host": "{{.Host}}",
    "basePath": "/",
    "paths": {
        "/api/v1/blocks/get-block/{sha1_hash}": {
            "get": {
                "description": "Given an SHA1 hash will return and validate data stored to disk in the form of a file",
                "produces": [
                    "application/octet-stream"
                ],
                "summary": "Returns a block of data as a file",
                "operationId": "get-block",
                "parameters": [
                    {
                        "type": "string",
                        "description": "da39a3ee5e6b4b0d3255bfef95601890afd80709",
                        "name": "sha1_hash",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "string"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.APIStatusMessage"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.APIStatusMessage"
                        }
                    }
                }
            }
        },
        "/api/v1/blocks/post-block": {
            "post": {
                "description": "Post a block of binary data between 1 byte and 64 MiB returning a SHA1 hash of the data posted is JSON format",
                "consumes": [
                    "multipart/form-data"
                ],
                "produces": [
                    "application/json"
                ],
                "summary": "Post a block of binary data",
                "operationId": "post-block",
                "parameters": [
                    {
                        "type": "file",
                        "description": "The block file you wish to upload",
                        "name": "block",
                        "in": "formData",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.SHA1Response"
                        }
                    },
                    "400": {
                        "description": "Bad Request",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.APIStatusMessage"
                        }
                    },
                    "409": {
                        "description": "Conflict",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.APIStatusMessage"
                        }
                    },
                    "500": {
                        "description": "Internal Server Error",
                        "schema": {
                            "type": "object",
                            "$ref": "#/definitions/models.APIStatusMessage"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "models.APIStatusMessage": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "integer",
                    "example": 200
                },
                "message": {
                    "type": "string",
                    "example": "Everything is great!'"
                }
            }
        },
        "models.SHA1Response": {
            "type": "object",
            "properties": {
                "code": {
                    "type": "integer",
                    "example": 200
                },
                "sha1-hash": {
                    "type": "string",
                    "example": "cf23df2207d99a74fbe169e3eba035e633b65d94"
                }
            }
        }
    }
}`

type swaggerInfo struct {
	Version     string
	Host        string
	BasePath    string
	Title       string
	Description string
}

// SwaggerInfo holds exported Swagger Info so clients can modify it
var SwaggerInfo swaggerInfo

type s struct{}

func (s *s) ReadDoc() string {
	t, err := template.New("swagger_info").Parse(doc)
	if err != nil {
		return doc
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, SwaggerInfo); err != nil {
		return doc
	}

	return tpl.String()
}

func init() {
	swag.Register(swag.Name, &s{})
}
