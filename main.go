package main

import (
	"./docs"
	"./models"
	"fmt"
	"github.com/dgraph-io/badger"
	"github.com/gin-contrib/size"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/gin-swagger"
	_ "github.com/swaggo/gin-swagger/swaggerFiles"
	"log"
	"net/http"
	"runtime"
)

// swagger comments

// @title Veritas coding exercise
// @version 1
// @description This API is part of a Veritas coding exercise regarding a REST API to write and retrieve blocks of data
// @contact.name Benjamin Knigge
// @contact.url https://odinsql.com/contact/
// @contact.email ben@heptec.com
// @BasePath /

// this type exists so that I don't have to pass the config and db around in the gin context and then cast them in my handlers
type App struct {
	config *Config
	db     *badger.DB
}

func (a *App) logIfVerbose(message string) {
	if a.config.Verbose {
		log.Println(message)
	}
}

// If you are going to allow api requests via a web browser to an api on a different domain you will have to deal with CORS
func setCors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Cache-Control", "no-cache")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "false")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
		c.Writer.Header().Set("Access-Control-Allow-Headers",
			"Authorization, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control")
	}
}

// gin will recover from an unhandled exception but our api should be returning json even in the case of an unhandled error
func (a *App) globalRecover(c *gin.Context) {
	defer func(c *gin.Context) {
		if rec := recover(); rec != nil {
			c.JSON(http.StatusInternalServerError, models.APIStatusMessage{Code: 500, Message: fmt.Sprintf("Internal Server Error, %v", rec)})
			// if verbose is true in the config print the error our to the console
			if a.config.Verbose {
				log.Println(fmt.Sprintf("Gin Global recovery! Error : %v", rec))
			}
		}
	}(c)
	c.Next()
}

// I've been working a lot on Ubuntu servers and typically I will configure a server to run as a service on linux
// I didn't do that in this case as it would add additional complexity.
// This is the package I normally would use  : https://github.com/kardianos/service
func main() {

	// load the config
	config, err := NewConfig()
	if err != nil {
		log.Println(fmt.Sprintf("CRASH! Failed to load the config! : %v", err.Error()))
		panic(err)
	}
	// I wanted to set the gomaxprocs prior to creating the db
	app := &App{config, nil}

	// set gomaxprocs if not set to auto in the config
	if !app.config.GoMaxProc.Auto {
		runtime.GOMAXPROCS(int(app.config.GoMaxProc.Override))
		app.logIfVerbose(fmt.Sprintf("GOMAXPROCS has been set to %v", app.config.GoMaxProc.Override))
	}

	// you used to have to set this with a declarative comment
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%v", app.config.Gin.Host, app.config.Gin.Port)

	//  open the db
	opts := badger.DefaultOptions
	opts.Dir = config.Badger.OptsDir
	opts.ValueDir = config.Badger.ValueLogDir
	db, err := badger.Open(opts)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	app.db = db

	// load the routes
	router := GetRouter(app)
	// sets CORS headers
	router.Use(setCors())
	// global recovery middleware to handle unhandled "500" errors
	router.Use(app.globalRecover)

	// If a file is uploaded far outside of the max acceptable upload size to any method I want it to fail
	// I will check the actual limits in the request handler

	router.Use(limits.RequestSizeLimiter(int64(config.Gin.MaxBlockSizeMib*2) << 20))

	// gin actually support 3 "modes" debug, release and, test
	// gin will provide additional warnings in debug mode
	if app.config.Gin.DebugMode {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	// I've used a a socket in the past as well when configuring a go server behind nginx.
	// Given time constraints and the additional complexity involved in configuring nginx
	// In this instance I've decided against it.
	// router.RunUnix("path to the socket file")

	router.Run(fmt.Sprintf("%s:%v", config.Gin.Host, config.Gin.Port))

}
